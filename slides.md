
![logo_easymov](images/logo_easymov_v.png)

<style>
img[alt=logo_easymov]{width: 25vw; box-shadow: none;border:0; margin: auto;}
</style>

---

![logocozmo](images/cozmo.png)

#### Forward or backward ?
> @@EasymovCozmo drive 2

<style>
img[alt=logocozmo]{box-shadow: none; border:0; margin: auto;}
</style>

---

![logo](images/isima.png)

![logo](images/isibot.png) ![logo](images/i2c.png)

![logo](images/logo_easymov_v.png)

<style>
img[alt=logo]{height: 250px; box-shadow: none;border:0; margin: auto;}
</style>

---

# Isaac Asimov

![asimov](http://cdn8.openculture.com/wp-content/uploads/2014/10/24100036/Isaac_Asimov_on_Throne.png)

<!--
Le pere de la robotique
Science-fiction | les robots | les lois de la robotique
Inventeur du mot robotique
-->

---

# Les trois lois de la robotique

1. un robot ne peut porter atteinte à un être humain, ni, en restant passif, permettre qu'un être humain soit exposé au danger ;
2. un robot doit obéir aux ordres qui lui sont donnés par un être humain, sauf si de tels ordres entrent en conflit avec la première loi ;
3. un robot doit protéger son existence tant que cette protection n'entre pas en conflit avec la première ou la deuxième loi.

---

# Les robots imaginés par Asimov

![cavesofsteel](images/robbi.jpg) ![cavesofsteel](images/cavesofsteel.jpg)

<style>
img[alt=cavesofsteel]{height: 350px;}
</style>

---

# La réalité

![realite](images/etrange.jpg)
![realite](images/etrange2.jpg)

<style>
img[alt=realite]{height: 50vh;}
</style>

---

# La réalité

![animation](images/robot1.gif)
![animation](images/robot2.gif)

![animation](images/robot3.gif)
![animation](images/robot4.gif)

<style>
img[alt=animation]{height: 250px;}
</style>

---

# Humanoïdes
## VS
# Objets robotisés

![vs](images/vs.png)

<style>
img[alt=vs]{width: 30vw; box-shadow: none; border:0; margin: auto;}
</style>


<!--
Il y a des courants de pensé
Ce qui veulent automatiser nos objet du quotidien
et ceux qui veulent créer des robots humanoide qui s'adapte a notre monde

On appartiens a aucun des deux pensée
soyons juste réaliste, la technologie ne nou permet pas aujourd'hui de faire un humaoide assez polyvalent
l'air de rien c'est une sécré machine l'humain

Je ne doute pas qu'un jour il y aura des robots humanoide capable de tout faire
mais on en ai pas la
donc aujourd'hui on fait ca
-->

---

# La (vraie) réalité

![roomba](images/roomba.gif)

<style>
img[alt=roomba]{height: 60vh;}
</style>

---

![logo](images/shadoks5.png)

# Pourquoi c'est compliqué ?

---

# 1. La technologie

<div class="container">
<div class="circle elec"><div class="text">Électronique</div></div>
<div class="circle info"><div class="text">Informatique</div></div>
<div class="circle meca"><div class="text">Mécanique</div></div>
</div>

<style>
.container
{
  position: relative;
  height: 600px;
  width: 600px;
  margin: auto;
}
.circle
{
  position: absolute;
  border-radius: 50%;
  width: 60%;
  height: 60%;
  font-family: montserrat;
  font-size: 1.5rem;
}
.circle .text
{
  position: relative;
  display: flex;
  align-items: center;
  height: 100%;
  justify-content: center;
}
.info
{
  top: 0;
  right: 50%;
  transform: translateX(50%);
  border: 2px solid rgba(255, 0, 0, 0.5);
  background: rgba(255, 0, 0, 0.2);
}
.elec
{
  bottom: 0;
  right: 0;
  border: 2px solid rgba(0, 255, 0, 0.5);
  background: rgba(0, 255, 0, 0.2);
}
.meca
{
  bottom: 0;
  left: 0;
  border: 2px solid  rgba(0, 0, 255, 0.5);
  background: rgba(0, 0, 255, 0.2);
}
</style>

<!--
Quand l'un de ces millieu avance
la robotique avance
Mais quand l'un d'eux stagne et bien la robotique plafonne

L'augmentation des puissance de calcul a permis de faire des choses extradordinaire
La reduction des prix des composants electronique et mecanique aussi
-->

---

# 2. La polyvalence

![mars](images/path_planning.png)

<style>
img[alt=mars]{box-shadow: none; border:0; margin: auto;}
</style>

---

# 3. Le prix

![logo](images/velodyne.png)
![logo](images/battery.png)

![logo](images/production.jpg)

---

# 4. La compatibilité

![willow](images/willow.png)

<style>
img[alt=willow]{height: 80vh; box-shadow: none; border:0; margin: auto;}
</style>

---

# Mais ça c'était avant !

---

# 1. L'avènement des spécialistes

#### Expertise dans chaque domaine

<div class="container">
<div class="circle elec"><div class="text">Électronique</div></div>
<div class="circle info"><div class="text">Informatique</div></div>
<div class="circle meca"><div class="text">Mécanique</div></div>
</div>

---

# 2. Simulation

#### Facilite et accélère le développement

<video class="video" controls src="videos/bezier2.webm">Ici la description alternative</video>

<style>
.video{height: 75vh;}
</style>

---

# 3. Open Source / Open Hardware

#### Réduit les coût de développement

![os](images/arduino.png)
![os](images/mbed.png)

![os](images/rplidar.png)
![os](images/turtlebot.png)

<style>
img[alt=os]{height: 15vh; box-shadow: none; border:0; margin: auto; margin: 1vw;}
</style>

---

# 4. ROS

#### Apporte la compatibilité entre robots

![ros](images/ros.png)

<style>
img[alt=ros]{box-shadow: none; border:0; margin: auto;}
</style>

---

# Nos projets

#### Robot assistance personnes agées

![robot](images/ubo.jpg)

---

# Nos projets

#### Tracteur électrique

![robot](images/sabiagri.jpg)

---

# Nos projets

#### Robot livraison dernier kilomètre

![robot](images/twinswheel.jpg)

<style>
img[alt=robot]{height: 50vh; box-shadow: none; border:0; margin: auto;}
</style>

---

# Nos projets

#### Robot distributeur de flyers

<video controls muted class="video">
  <source src="videos/leonardo.mp4" type="video/mp4">
</video>

---

# Nos projets

#### Détection de palette

<video class="video" controls src="videos/demo_pallets.mp4"></video>

---

# Nos projets

#### Tracking de personne

<video class="video" controls src="videos/demo_tracking.mp4"></video>

---

# Dans la robotique
# Il y a du boulot !

---

# Dans le bas-niveau

#### Missions

- Interface bas-niveau avec des capteurs
- Conception de capteurs
- Contrôle d'actionneurs

#### Technologies

- Microcontrôleur
- FPGA
- C

---

# Dans le haut-niveau

#### Missions

- Algorithmes (perception, localisation, navigation)
- Cloud (communication et gestion multirobot)
- Apprentissage
- UI/UX

#### Technologies

- ROS
- C++ / Python
- Intelligence Artificielle / Réseaux neuronaux
- Cryptographie / Sécurité

---

# La robotique de demain ?

<div class="robot-container">
  <div>
    <h4>Expressivité</h4>
    <img src="images/nexi1.jpg" alt="robotdemain"/>
  </div>
  <div>
    <h4>Coopération</h4>
    <img src="images/robotswarm.jpg" alt="robotdemain"/>
  </div>
  <div>
    <h4>Connectivité</h4>
    <img src="images/cloud.jpg" alt="robotdemain"/>
  </div>
</div>

<style>
img[alt=robotdemain]{height: 300px; box-shadow: none; border:0; margin: auto;}

.robot-container
{
  display: flex;
  justify-content: center;
}
</style>

---

# Kit du débutant en robotique

- Allez à **ISIBOT**, mettez les mains dans le cambouis !
- Non à l'*Arduino*, oui à la *Nucleo*
- ROS, OpenCV et Linux

---

# Easymov propose des stages !

![robot](images/stage.jpg)

<i class="fa fa-envelope" aria-hidden="true"></i>
[careers@@easymov.fr](mailto:careers@@easymov.fr)

<i class="fa fa-globe" aria-hidden="true"></i>
[easymov.fr/fr/careers](http://easymov.fr/fr/careers)

---

# Contact

<i class="fa fa-envelope" aria-hidden="true"></i>
[contact@@easymov.fr](mailto:contact@@easymov.fr)

<i class="fa fa-twitter" aria-hidden="true"></i>
[@@easymovrobotics](http://twitter.com/easymovrobotics)

<i class="fa fa-globe" aria-hidden="true"></i>
[www.easymov.fr](http://www.easymov.fr)

<i class="fa fa-gitlab" aria-hidden="true"></i>
[easymov.gitlab.io/recre_anelis](http://easymov.gitlab.io/recre_anelis)

<!--
Premier robots avec de fausses émotion mais qui ont l'air tellement vrai

Les robots aujourd'hui son très con

robbie c'est vraiment le premier robot qui a des reaction humaine
la réalité c'est que l'on s'en fou si le robot eprouvé des sentiments ce qui nous interesse tout les jours c'est d'arriver a faire croire que le robot a des emotions
est triste, est contents.

A une action faite par un humain on veux percevoir la reaction attendu par un robot.
Sans ca un robot reste une machine au meme niveau qu'un four micro onde.

PIXAR => donner de l'emotion a des personnage comme wall-e.

La robotique a un avenir radieux devant elles
La science-fiction a imaginé le pire comme le meilleur de ce que cela pourrait devenir.

Meme si vous avec des envie de domination mondiale, la robotique c'est ouvert a tous.
-->
